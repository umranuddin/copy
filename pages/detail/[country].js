
import React, { useState } from 'react'
import Head from 'next/head';
import { useEffect } from 'react';
import Image from 'next/image';

// export const getStaticPaths = async () => {
//     const res = await fetch("https://restcountries.com/v3.1/all");
//     const data = await res.json();
   
//     const paths = data.map((ele) => {
//       return {
//         params: {
//           Countrydetails: ele.cca3.toString(),
//         }
//       }
//     });
  
//     return {
//       paths,
//       fallback: false,
//     }
//   }
  
export async function getServerSideProps(context)  {
   const {country} = context.query
   
    const res = await fetch(`https://restcountries.com/v3.1/alpha/` + `${country}`);
    const data = await res.json();
   
    return {
      props: { data }
    };
  }
  const country=({data}) => {
  
   const [myData, setMyData] = useState([])
    useEffect(() => {
    tempFun()
    }, [])

  const tempFun = async () => {
    let result = [];

    if(data[0].borders){
      await Promise.all(
        data[0].borders.map(async (ele) => {
          const res1 = await fetch('https://restcountries.com/v3.1/alpha/' + `${ele}`);
          const data1 = await res1.json();
          console.log(data1)

          result.push(data1[0]);
        })
      )
    }

    setMyData(result)
  }
  let curr = " ";
     if(data && data[0].currencies){
    curr = Object.values(data[0].currencies)[0].name;
     }

  return (
  <>
  <Head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous"/>


  </Head>
  
    <div className='container mt-2'>
        <div className="row justify-content-center">
          <div className="col-sm-8  border">
            <h1> {data[0].name.common} </h1>
            <div className="row">
              <div className="col-sm-5 mt-2">
                <Image src={data[0].flags.png} width={500} height={500} alt="flag" />
              </div>
              <div className="col-sm mt-1">
                <p className="mb-2"><b>Native Name :</b> {
                   data[0].name.nativeName && Object.values(data[0].name.nativeName).map((ele ,index)=>{
                    return <span key= {index}>{ele.common} </span>
                  })
                } </p>
                <p className="mb-2"><b>Capital :</b>  {data[0].capital && data[0].capital[0]}</p>
                <p className="mb-2"><b>Population :</b>  {data[0].population}</p>
                <p className="mb-2"><b>Region :</b>  {data[0].region}</p>
                <p className="mb-2"><b>Sub-region :</b> {data[0].subregion}</p>
                <p className="mb-2"><b>Area :</b>  {data[0].area}</p>
                <p className="mb-2"><b>Country Code</b> : {data[0].region}</p>
                <p className="mb-2"><b>Languages :</b> {
                data[0].languages && Object.values(data[0].languages).map((ele,index)=>{
                  return <span key={index}>{ele} </span>
                })
                }</p>
                <p className="mb-2"><b>Currencies :</b> {curr}</p>
                <p className="mb-2"><b>Timezones :</b> {data[0].timezones  && data[0].timezones[0]}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div className='container mt-2'>
        <div className="row justify-content-center">
          <div className="col-sm-8  border">
            <h2>Neighbour Countries</h2>
            { 
            myData.map((ele, index)=>{
              return <span style={{'margin':'20px'}} key={index} >
               <Image src={ele.flags.png} width={200} height={200}alt="flag"/>
              </span>
            })  
            }
          </div>
        </div>
      </div>
      </>
  )
          
}
export default country;